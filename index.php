<?php

session_start();

function error ($message) {
    header('HTTP/1.0 400 Bad Request', true, 400);
    echo json_encode(['error' => $message]);
    die();
}

// https://www.yelp.com/developers/documentation/v3/get_started
function search($params = [])
{
    if (!isset($params['longitude'])) {
        error('No location set!');
    }

    $params['limit'] = 30;
    $params['categories'] = 'restaurants';
    $params['open_now'] = $_POST['open_now'] === 'on';

    $key = '👋👋👋👋 __ADD__❗️❗️_YELP__❗️❗️___KEY_❗️❗️___HERE__❗️❗️_ 👋👋👋👋';
    $url = 'https://api.yelp.com/v3/businesses/search?' . http_build_query($params);

    $curl = curl_init();
    curl_setopt_array($curl, [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HTTPHEADER => [
            "authorization: Bearer " . $key,
            "cache-control: no-cache",
        ]
    ]);

    $response = curl_exec($curl);
    $httpStatus = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    curl_close($curl);

    if (false === $response || 200 !== $httpStatus) {
        error(curl_error($curl) . curl_errno($curl) . $httpStatus);
    }

    return $response;
}

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    if (isset($_POST['longitude'])) {
        $_SESSION['location'] = $_POST;
    }
    if (isset($_POST['sort_by'])) {
        $params = array_merge($_POST, $_SESSION['location']);
        echo search($params);
    }
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width">
    <title>Food Jackpot</title>
    <style>
        body {
            --color: #d32323;
            --box-size: 7rem;
            min-height: 100vh;
            font-family: 'Helvetica Neue', Helvetica;
            color: white;
            margin: 0;
            text-align: center;
        }
        form {
            background-image: linear-gradient(#c41200, darkred);
            padding: 1rem;
            font-size: 90%;
        }
        button {
            margin-left: 2rem;
        }
        @keyframes opa {
            to {
                opacity: 0;
            }
        }
        main div {
            display: inline-block;
            height: var(--box-size);
            width: var(--box-size);
            margin: 1rem;
            animation: opa alternate infinite;
            position: relative;
            border-radius: .3rem;
            box-shadow: 0 0 .5rem #00000066;
            overflow: hidden;
        }
        main div a {
            display: inline-flex;
            align-items: center;
            height: var(--box-size);
            text-decoration: none;
            color: white;
        }
        .price, .rating {
            position: absolute;
            top: .2rem;
            font-size: 80%;
        }
        .price {
            right: .3rem;
        }
        .rating {
            left: .3rem;
        }
        .loser, .winner {
            animation: none;
        }
        .loser {
            z-index: -1;
            opacity: 0;
        }
        .winner {
            opacity: 1;
        }
        .error {
            position: absolute;
            top: 0;
            left: 0;
            width: 100vw;
            height: 100vh;
            background: rgba(0, 0, 0, .4);
            display: flex;
            align-items: center;
            justify-content: space-evenly;
        }
    </style>
</head>
<body>
<form>
    <select name="price">
        <?php foreach ([1 => '&euro;', 2 => '&euro;&euro;', 3 => '&euro;&euro;&euro;', 4 => '&euro;&euro;&euro;&euro;', '' => ''] as $value => $name) : ?>
            <option value="<?= $value ?>"><?= $name ?></option>
        <?php endforeach; ?>
    </select>
    <select name="sort_by">
        <?php foreach (['rating', 'best_match', 'review_count', 'distance'] as $sort) : ?>
            <option value="<?= $sort ?>"><?= $sort ?></option>
        <?php endforeach; ?>
    </select>
    <select name="radius">
        <?php for ($i=5; $i<20; $i++) : ?>
            <option value="<?= $i*100 ?>"><?= $i ?>00m</option>
        <?php endfor; ?>
    </select>
    <input type="checkbox" checked name="open_now" id="opennow"><label for="opennow">Open</label>
    <button type="button">GO</button>
</form>

<main></main>

<script>
  const setPosition = position => {
    let data = new FormData();
    data.append('longitude', position.coords.longitude);
    data.append('latitude', position.coords.latitude);
    fetch('index.php', {
      method: 'POST',
      body: data,
      credentials: 'same-origin'
    })
  };

  const showError = (mssg) => {
    let error = document.createElement('div');
    error.classList.add('error');
    error.innerHTML = mssg !== undefined ? mssg: 'Sorry, it is not possible to find your location! Better try a different browser.';
    let close = document.createElement('button');
    close.innerHTML = 'close';
    close.onclick = () => { error.parentNode.removeChild(error) };
    error.appendChild(close);
    document.body.appendChild(error);
  };

  if (<?=!isset($_SESSION['location']) ? 'true' : 'false'?>) {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(setPosition, showError);
    } else {
      showError();
    }
  }

  const selectWinner = () => {
    let options = document.querySelectorAll('main div');
    options.forEach(option => { option.classList.add('loser') });
    options[Math.floor(Math.random() * options.length)].classList.replace('loser', 'winner');
  };

  const addBusiness = business => {
    let div = document.createElement('div');
    div.style.animationDuration = Math.random() + 's';
    div.style.background = 'hsl(' + Math.random() * 360 + ', 100%, 40%)';
    div.innerHTML = '<span class="rating">' + business.rating + '★</span>' + (business.price !== undefined ? ' <span class="price">' + business.price + '</span>' : '')
      + '<a href="' + business.url + '" data-business-id="' + business.id + '" target="_blank" rel="noopener noreferrer">'
      + business.name + '<br>(' + Math.round(business.distance) + 'm)</a>';
    document.querySelector('main').appendChild(div);
  };

  document.querySelector('button').onclick = () => {
    document.querySelector('main').innerHTML = '';
    data = new FormData(document.querySelector('form'));
    data.append('locale', navigator.language.replace('-', '_'));
    fetch('index.php', {
      method: 'POST',
      credentials: 'same-origin',
      body: data
    }).then(response => response.json())
      .then(response => {
        if (response.error !== undefined) {
          showError(response.error);
          return;
        }
        response.businesses.forEach(business => addBusiness(business));
        if (response.businesses.length) {
          setTimeout(selectWinner, 3000);
        } else {
          showError('What shithole are you in?')
        }
      })
      .catch(reason => showError(reason));
  };
</script>
</body>
</html>
